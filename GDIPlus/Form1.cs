﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace GDIPlus
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnDesenhar_Click(object sender, EventArgs e)
        {
            //criar uma folha em branco
            Bitmap folha = new Bitmap(picture.Width, picture.Height);

            //criar um desenhador
            Graphics desenhador = Graphics.FromImage(folha);

            //utilizar o desenho ou imagem
            desenhador.Clear(Color.White);

            #region Linhas
            //Brush pincel1 = new SolidBrush(Color.Red);
            //Pen lapis1 = new Pen(pincel1, 5);
            //Point ponto1 = new Point(100, 200);
            //Point ponto2 = new Point(300, 100);

            //Pen lapis2 = new Pen(Color.Black, 10);
            //Point ponto3 = new Point(0, 0);
            //Point ponto4 = new Point(100, 200);

            ////desenhador.DrawLine(lapis1, ponto1, ponto2);
            ////desenhador.DrawLine(lapis2, ponto3, ponto4);

            //Point[] pontos =
            //{
            //    new Point(50, 50),
            //    new Point(250, 50),
            //    new Point(250, 150),
            //    new Point(100, 80),
            //    new Point(85, 140)
            //};

            //desenhador.DrawLines(lapis2, pontos);
            #endregion

            #region Retângulos
            //Pen lapis1 = new Pen(Color.Green, 6);
            //Pen lapis2 = new Pen(Color.Red, 5);
            //Rectangle retangulo = new Rectangle(100, 50, 300, 200);

            //desenhador.DrawRectangle(lapis1, retangulo);
            //desenhador.DrawRectangle(lapis2, 0, 0, 140, 160);

            //Rectangle[] retangulos =
            //{
            //    new Rectangle(0,0,120,160),
            //    new Rectangle(20,30,70,160),
            //    new Rectangle(140,50,100,60),
            //    new Rectangle(55,40,22,60)
            //};

            //desenhador.DrawRectangles(lapis1, retangulos);
            //Brush pincel1 = new SolidBrush(Color.Green);
            //Brush pincel2 = new LinearGradientBrush(retangulo, Color.Red, Color.Green, 45);
            //desenhador.FillRectangle(pincel2, retangulo);
            //desenhador.FillRectangles(pincel2 , retangulos);
            #endregion

            #region Elipses e Círculos
            //Pen lapis1 = new Pen(Color.Green, 5);
            //Brush pincel1 = new SolidBrush(Color.Gray);

            //Rectangle retangulo1 = new Rectangle(10, 20, 160, 100);
            //desenhador.DrawEllipse(lapis1, retangulo1);
            //desenhador.DrawRectangle(lapis1 , retangulo1);
            //desenhador.FillEllipse(Brushes.AliceBlue, retangulo1);
            #endregion
            #region Polígonos
            //Pen lapis1 = new Pen(Color.Blue, 5);

            //Point[] pontos =
            //{
            //    new Point(100,100),
            //    new Point(300,200),
            //    new Point(250,250),
            //    new Point(100,260)
            //};

            ////desenhador.DrawPolygon(lapis1, pontos);
            //Rectangle retangulo = new Rectangle(0, 0, 120, 160);
            //Brush pincel1 = new LinearGradientBrush(retangulo, Color.Yellow, Color.Gray, 45);
            //desenhador.FillPolygon(pincel1, pontos);
            #endregion
            #region Curvas
            //Pen lapis = new Pen(Color.Brown, 5);

            //Point[] pontos =
            //{
            //    new Point(100,50),
            //    new Point(200,150),
            //    new Point(300,100),
            //    new Point(500,250),
            //    new Point(300,300)
            //};

            //desenhador.DrawCurve(lapis, pontos, 1.5f);
            //desenhador.DrawClosedCurve(lapis, pontos, 1.5f, FillMode.Alternate);
            //desenhador.FillClosedCurve(Brushes.Red, pontos, FillMode.Winding, 1.5f);
            #endregion
            #region Arcos
            //Pen lapis1 = new Pen(Color.Blue, 5);
            //Pen lapis2 = new Pen(Color.Brown, 5);
            //Rectangle retangulo = new Rectangle(100, 100, 200, 200);

            //desenhador.DrawRectangle(lapis1, retangulo);
            //desenhador.DrawArc(lapis2, retangulo, 45f, 90f);
            #endregion
            #region Bezier
            //Pen lapis = new Pen(Color.Green, 6);
            //Point p1 = new Point(50, 300);
            //Point p2 = new Point(200, 400);
            //Point p3 = new Point(300, 10);
            //Point p4 = new Point(500, 200);

            //Point[] pontos =
            //{
            //    new Point(50, 300),
            //    new Point(150, 350),
            //    new Point(300, 100),
            //    new Point(400, 150),
            //    new Point(500, 400),
            //    new Point(550, 10),
            //    new Point(600, 100)
            //};

            //desenhador.DrawBezier(lapis, p1, p2, p3, p4);
            //desenhador.DrawBeziers(lapis, pontos);
            #endregion
            #region Draw Pie
            //Pen lapis = new Pen(Color.Green, 5);
            //Rectangle retangulo = new Rectangle(50, 50, 300, 300);

            //desenhador.DrawPie(lapis, retangulo, 0f, 90f);
            //desenhador.FillPie(Brushes.Red, retangulo, 270f, 90f);
            //desenhador.DrawRectangle(lapis, retangulo);
            #endregion
            #region Path
            //Pen lapis = new Pen(Color.Black, 5);
            //GraphicsPath grafico = new GraphicsPath(FillMode.Alternate);

            //grafico.AddEllipse(new RectangleF(10, 10, 100, 150));
            //grafico.AddEllipse(new RectangleF(50, 10, 100, 150));
            //grafico.AddRectangle(new RectangleF(120, 50, 150, 100));

            //desenhador.DrawPath(lapis, grafico);
            //desenhador.FillPath(Brushes.Brown, grafico);
            #endregion
            #region Draw Strings
            //String texto = "São panquecas, panquecas de toucinho. Põe o bacon na panqueca direitinho. São panquecas, panquecas de toucinho que eu vou fazer.";
            //Font fonte = new Font("Arial", 28, FontStyle.Bold, GraphicsUnit.Point);
            //Point ponto = new Point(10, 20);
            //Brush pincel1 = new SolidBrush(Color.Green);
            //Brush pincel2 = new LinearGradientBrush(new Rectangle(0, 0, 400, 400), Color.Blue, Color.Yellow, 45);
            //Rectangle retangulo = new Rectangle(10, 20, 550, 350);
            //StringFormat alinhamento = new StringFormat();
            //desenhador.DrawString(texto, fonte, pincel1, ponto);
            //desenhador.DrawRectangle(new Pen(Color.Black, 5), retangulo);

            //alinhamento.Alignment = StringAlignment.Center;
            //alinhamento.LineAlignment = StringAlignment.Near;
            //alinhamento.FormatFlags = StringFormatFlags.DirectionVertical;

            //desenhador.DrawString("Titulo da página", fonte, pincel1, retangulo, alinhamento);
            //desenhador.DrawString(texto, fonte, pincel2, new RectangleF(10, 80, 550, 300), alinhamento);
            #endregion
            #region Imagens
            Image imgOrigem1 = Image.FromFile(Application.StartupPath + @"\imagens\timao-pumba.jpg");
            Rectangle origem1 = new Rectangle(0, 0, imgOrigem1.Width, imgOrigem1.Height);
            Rectangle destino1 = new Rectangle(0, 0, picture.Width, picture.Height);

            Image imgOrigem2 = Image.FromFile(Application.StartupPath + @"\imagens\scooby.jpg");
            Rectangle origem2 = new Rectangle(0, 0, 280, 370);
            Rectangle destino2 = new Rectangle(240, 40, 280, 370);

            desenhador.DrawImage(imgOrigem1, destino1, origem1, GraphicsUnit.Pixel);
            desenhador.DrawImage(imgOrigem2, destino2, origem2, GraphicsUnit.Pixel);
            #endregion
            //utilizar num picture box
            picture.BackgroundImage = folha;


            //salvar a imagem
            folha.Save("c:\\curso\\desenho.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);

        }
    }
}
